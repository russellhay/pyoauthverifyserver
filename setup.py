__author__ = "Russell Hay"

from setuptools import setup

setup(
    name="pyoauthverifyserver",
    description="A simple server to receive an OAuth Verification to simplify asking permission for desktop and console applications",
    version='0.1.2',
    py_modules=['oauthverifyserver'],
    author=__author__,
    author_email="me@russellhay.com",
    url="https://bitbucket.org/russellhay/pyoauthverifyserver",
)
