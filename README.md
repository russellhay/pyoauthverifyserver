Example Usage:

    :::python
        from tumblpy import Tumblpy
        from keys import CONSUMER_KEY, SECRET_KEY
        from oauthverifyserver import OAuthCallbackServer

        SECRET = None
        TOKEN = None

        def update(verify, token):
            global SECRET, TOKEN
            oauth_token = token[0]
            oauth_verify = verify[0]

            t = Tumblpy(CONSUMER_KEY, SECREY_KEY, None, SECRET)
            authorized_tokens = t.get_authorized_tokens(oauth_verify)

            # Store these two values somewhere
            TOKEN = authorized_tokens['oauth_token']
            SECRET = authorized_tokens['oauth_token_secret']


        t = Tumblpy(CONSUMER_KEY, SECRET_KEY)
        server = OAuthCallbackServer(8080, update)
        props = t.get_authentication_tokens(callback_url='http://localhost:8080')
        url = auth_props['auth_url']
        SECRET = auth_props['oauth_token_secret']

        webbrowser.open(url)
        callback_server.wait()
        print("Record these values:")
        print("oauth_token={0}".format(TOKEN))
        print("oauth_token_secret={0}".format(SECRET))
        # You are now authenticated.
